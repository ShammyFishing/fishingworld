using UdonSharp;
using UnityEngine;
using UnityEngine.UI;
using VRC.SDKBase;
using VRC.Udon;

public class Hook : UdonSharpBehaviour
{
    public HookSettings settings;
    public Slider reelingSlider;
    public RectTransform reelingGreen, reelingHandle;

    private FishingPole fishingPole;
    private Fish fishOnHook;
    private float greenRadius;
    private float greenPos;

    [HideInInspector]
    public int Power;
    private int TimePower;
    [HideInInspector]
    public FishingSpot catchingFrom;

    private float timeSinceUpdate;
    private const float updateInterval = 1f;
    private const int bucketLayer = 23;
    private bool reelingFacingLeft;
    void Start()
    {
    }

    void FixedUpdate() {
        if (Networking.IsOwner(Networking.LocalPlayer, gameObject)) {
            if (catchingFrom != null && transform.childCount == 0) {
                timeSinceUpdate -= Time.deltaTime;
                if (fishOnHook == null && timeSinceUpdate < 0 ) {
                    timeSinceUpdate = updateInterval;
                    var fish = catchingFrom.TryCatchFish(GetPowerTotal());
                    if (fish != null) {
                        fishOnHook = fish;
                        SetReeling();
                    } else if (TimePower < 20) {
                        TimePower++;
                    }
                } else if (fishOnHook != null) {
                    var changeDirection = Random.Range(0, 100) > 97 ||
                        reelingSlider.value <= 0 && reelingFacingLeft ||
                        reelingSlider.value >= 1 && !reelingFacingLeft;
                    if (changeDirection) {
                        reelingFacingLeft = !reelingFacingLeft;
                    }
                    reelingHandle.localRotation = new Quaternion(0f, reelingFacingLeft ? 0f : 1f, 0f, 0f);

                    var step = 0.5f * Time.deltaTime;
                    reelingSlider.value = reelingFacingLeft 
                        ? reelingSlider.value - step
                        : reelingSlider.value + step;
                }
            }
        }
    }

    void SetReeling() {
        reelingSlider.gameObject.SetActive(true);
        greenRadius = Random.Range(.05f, .2f);
        greenPos = Random.Range(greenRadius, 1 - greenRadius);
        reelingSlider.value = Random.Range(0, 1);

        reelingGreen.anchorMax = new Vector2(greenPos + greenRadius, 1);
        reelingGreen.anchorMin = new Vector2(greenPos - greenRadius, 0);
    }

    public int GetPower() {
        return fishingPole.Power;
    }

    public int GetPowerTotal() {
        return TimePower + fishingPole.Power;
    }

    private void OnTriggerEnter(Collider other)
    {
        if (other == null) {
            return;
        }
        // Debug.Log("OnTriggerEnter " + other.gameObject.name + ", Layer: " + other.gameObject.layer);
        if (other.gameObject.layer == bucketLayer && transform.childCount > 0) {
            // TODO: store in inventory / log
            Destroy(transform.GetChild(0).gameObject);
        } else if (other.gameObject.layer > bucketLayer) {
            var spot = GetFishingSpot(other.gameObject.layer);
            // Debug.Log("Set fishingSpot " + spot.name);
            if (spot != null) {
                catchingFrom = spot;
            }
        }
    }

    private void OnTriggerExit(Collider other)
    {
        if (other == null) {
            return;
        }

        var spot = GetFishingSpot(other.gameObject.layer);
        if (spot != null && spot.Layer == catchingFrom.Layer) {
            if (fishOnHook && Mathf.Abs(greenPos - reelingSlider.value) < greenRadius) {
                FishCaught(fishOnHook);
            }

            reelingSlider.gameObject.SetActive(false);
            catchingFrom = null;
            fishOnHook = null;
            TimePower = 0;
        }
    }

    public void FishCaught(Fish fish) {
        var fishObj = VRCInstantiate(fish.Prefab);
        fishObj.transform.SetParent(gameObject.transform);
        fishObj.transform.localPosition = new Vector3();
    }

    private FishingSpot GetFishingSpot(int layer) 
    {
        foreach(var spot in settings.fishingSpots) {
            if (spot.Layer == layer) {
                return spot;
            }
        }
        return null;
    }

    public void SetFishingPole(FishingPole pole) {
        fishingPole = pole;
    }
}
