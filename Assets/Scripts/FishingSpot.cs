
using UdonSharp;
using UnityEngine;
using VRC.SDKBase;
using VRC.Udon;
using System.Linq;

public class FishingSpot : UdonSharpBehaviour
{
    public int Layer;
    public Fish[] fishArray;

    void Start() {
    }

    public Fish TryCatchFish(int power) {
        foreach(var fish in fishArray) {
            var rand = Random.Range(0, power);
            Debug.Log("Trying to catch " + fish.name + " - " + rand +">"+ fish.PowerRequired + "=" + (rand > fish.PowerRequired));

            if (rand > fish.PowerRequired) {
                return fish;
            }
        }
        return null;
    }
}