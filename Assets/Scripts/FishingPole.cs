
using UdonSharp;
using UnityEngine;
using UnityEngine.UI;
using VRC.SDKBase;
using VRC.Udon;

public class FishingPole : UdonSharpBehaviour
{
    
    public Hook hook;
    public Text powerDisplay, fishingInDisplay, hookedDisplay;
    public int Power;

    public bool isHeld;

    void Start()
    {
        hook.SetFishingPole(this);
    }

    void Update() {
        powerDisplay.text = hook.GetPower().ToString();
        fishingInDisplay.text = hook.catchingFrom != null ? hook.catchingFrom.name : "";

        if (Networking.LocalPlayer != null) {
            var poleOwner = Networking.GetOwner(gameObject);
            var hookOwner = Networking.GetOwner(hook.gameObject);
            hookedDisplay.text = poleOwner.displayName + poleOwner.playerId + " / " + hookOwner.displayName + hookOwner.playerId;
        }
    }

    public override void OnPickupUseDown() {
        Networking.SetOwner(Networking.GetOwner(gameObject), hook.gameObject);
    }
}
